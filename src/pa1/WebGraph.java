package pa1;

import api.Graph;
import api.TaggedVertex;

import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of a WebGraph for use with the Crawler and Index class
 *
 * @author Zach Johnson and Alex Bjerke
 */
public class WebGraph<E> implements Graph
{
    private ArrayList<TaggedVertex<E>> taggedVertexList;
    private ArrayList<E> vertexList;
    private ArrayList<ArrayList<Integer>> outgoingLists;
    private ArrayList<ArrayList<Integer>> incomingLists;
    private ArrayList<Integer> depth;

    WebGraph(){
        this.taggedVertexList = new ArrayList<>();
        this.vertexList = new ArrayList<>();
        this.outgoingLists = new ArrayList<>();
        this.incomingLists = new ArrayList<>();
        this.depth = new ArrayList<>();
    }

    /**
     * Returns an ArrayList of the actual objects constituting the vertices
     * of this graph.
     * @return
     *   ArrayList of objects in the graph
     */
    public ArrayList<E> vertexData() {
        return vertexList;
    }

    /**
     * Returns an ArrayList that is identical to that returned by vertexData(), except
     * that each vertex is associated with its incoming edge count.
     * @return
     *   ArrayList of objects in the graph, each associated with its incoming edge count
     */
    public ArrayList<TaggedVertex<E>> vertexDataWithIncomingCounts() {
        return taggedVertexList;
    }

    /**
     * Returns a list of outgoing edges, that is, a list of indices for neighbors
     * of the vertex with given index.
     * This method may throw ArrayIndexOutOfBoundsException if the index
     * is invalid.
     * @param index
     *   index of the given vertex according to vertexData()
     * @return
     *   list of outgoing edges
     */
    public List<Integer> getNeighbors(int index) {
        return outgoingLists.get(index);
    }

    /**
     * Returns a list of incoming edges, that is, a list of indices for vertices
     * having the given vertex as a neighbor.
     * This method may throw ArrayIndexOutOfBoundsException if the index
     * is invalid.
     * @param index
     *   index of the given vertex according to vertexData()
     * @return
     *   list of incoming edges
     */
    public List<Integer> getIncoming(int index) {
        return incomingLists.get(index);
    }

    /**
     * Returns the list of incoming lists
     * @return incomingLists
     */
    ArrayList<ArrayList<Integer>> getIncomingLists() {
        return incomingLists;
    }

    /**
     * Returns the list of outgoing lists
     * @return outgoingLists
     */
    ArrayList<ArrayList<Integer>> getOutgoingLists() {
        return outgoingLists;
    }

    /**
     * Adds indexChild to the outgoingList of indexParent
     * and indexParent to the incomingList of indexChild
     * @param indexParent index of the parent link
     * @param indexChild index of the child link
     */
    void addNeighbor(int indexParent, int indexChild) {
            outgoingLists.get(indexParent).add(indexChild);
            incomingLists.get(indexChild).add(indexParent);
            TaggedVertex<E> newChild = new TaggedVertex<>(vertexList.get(indexChild), taggedVertexList.get(indexChild).getTagValue() + 1);
            taggedVertexList.set(indexChild, newChild);

    }

    /**
     * Returns the depth of the link
     * @param index index of the link in vertexData
     * @return depth of the link in the graph
     */
    int getDepth(int index) {
        return depth.get(index);
    }

    /**
     * Sets the depth of this link
     * @param index index of the link
     * @param depth new depth of the link
     */
    void setDepth(int index, int depth) {
        if (this.depth.size() <= index) {
            this.depth.add(depth);
        }
        else {
            this.depth.set(index, depth);
        }
    }
}