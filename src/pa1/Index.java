package pa1;

import java.io.IOException;
import java.util.*;

import api.TaggedVertex;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.UnsupportedMimeTypeException;
import org.jsoup.nodes.Document;

import static api.Util.*;


/**
 * Implementation of an inverted index for a web graph.
 * 
 * @author Zach Johnson and Alex Bjerke
 */
public class Index {

  private HashMap<String, Integer[]> indexHashMap;
  private List<TaggedVertex<String>> pages;

  /**
   * Constructs an index from the given list of urls.  The
   * tag value for each url is the indegree of the corresponding
   * node in the graph to be indexed.
   *
   * @param urls information about graph to be indexed
   */
  public Index(List<TaggedVertex<String>> urls) {
    indexHashMap = new HashMap<>();
    pages = urls;
  }

  /**
   * Creates the index.
   */
  public void makeIndex() {
    int numPages = pages.size();
    String url;
    String bodyText;
    String curWord;
    Integer[] intArr;
    for (int p = 0; p < numPages; p++) {
      if (p % 50 == 0) {
        try {
          Thread.sleep(3000);
        } catch (InterruptedException ignore) {
        }
      }
      url = pages.get(p).getVertexData();
      Document doc = new Document("");
      try {
        doc = Jsoup.connect(url).get();
      }
      catch (UnsupportedMimeTypeException e)
      {
        System.out.println("--unsupported document type, do nothing");
      }
      catch (HttpStatusException e)
      {
        System.out.println("--invalid link, do nothing");
      }
      catch (IOException e) {
        e.printStackTrace();
      }

      if(doc.body() != null){
          bodyText = doc.body().text();
          Scanner scanner = new Scanner(bodyText).useDelimiter("\\s");
          while (scanner.hasNext()) {
              curWord = stripPunctuation(scanner.next());
              if (!isStopWord(curWord) && (!curWord.isEmpty())) {
                  intArr = indexHashMap.get(curWord);
                  if (intArr == null) {
                      intArr = new Integer[numPages];  //replace above line

                      intArr[p] = 1;
                      indexHashMap.put(curWord, intArr);
                  }
                  else {
                      if(intArr[p] == null){
                        intArr[p] = 1;
                      }
                      else{
                        intArr[p] += 1;
                      }
                  }
              }
          }
      }
    }
  }

  /**
   * Searches the index for pages containing keyword w.  Returns a list
   * of urls ordered by ranking (largest to smallest).  The tag
   * value associated with each url is its ranking.
   * The ranking for a given page is the number of occurrences
   * of the keyword multiplied by the indegree of its url in
   * the associated graph.  No pages with rank zero are included.
   *
   * @param w keyword to search for
   * @return ranked list of urls
   */
  public List<TaggedVertex<String>> search(String w) {
    if (w == null) {
      return null;
    }
    Integer[] numTimesWordInPageArr = indexHashMap.get(w);
    List<TaggedVertex<String>> rankedListUnsorted = new ArrayList<>();

    //if the word was never found, return null
    if (numTimesWordInPageArr == null) {
      return rankedListUnsorted;
    }

    int numPossiblePages = pages.size();
    TaggedVertex<String> curPageTaggedVertex;
    int pageInDegree;
    int ranking;

    //iterate through the Integer array that represents how many times w appears in each page
    for (int i = 0; i < numPossiblePages; i++) {
      if (numTimesWordInPageArr[i] != null) {
        curPageTaggedVertex = pages.get(i);
        pageInDegree = curPageTaggedVertex.getTagValue();
        Integer  numTimesWordInPage1 = numTimesWordInPageArr[i];
        if(numTimesWordInPage1 == null){
          numTimesWordInPage1 = 0;
        }
        ranking = (pageInDegree) * (numTimesWordInPage1);
        if (ranking > 0) {
          rankedListUnsorted.add(new TaggedVertex<>(curPageTaggedVertex.getVertexData(), ranking));
        }
      }
    }
    return getSortedList(rankedListUnsorted);
  }

  /**
   * Searches the index for pages containing both of the keywords w1
   * and w2.  Returns a list of qualifying
   * urls ordered by ranking (largest to smallest).  The tag
   * value associated with each url is its ranking.
   * The ranking for a given page is the number of occurrences
   * of w1 plus number of occurrences of w2, all multiplied by the
   * indegree of its url in the associated graph.
   * No pages with rank zero are included.
   *
   * @param w1 first keyword to search for
   * @param w2 second keyword to search for
   * @return ranked list of urls
   */
  public List<TaggedVertex<String>> searchWithAnd(String w1, String w2) {

    if (w1 == null || w2 == null) {
      return null;
    }
    Integer[] numTimesWordInPageArr1 = indexHashMap.get(w1);
    Integer[] numTimesWordInPageArr2 = indexHashMap.get(w2);
    List<TaggedVertex<String>> rankedListUnsorted = new ArrayList<>();

    //if the word was never found, return empty
    if ((numTimesWordInPageArr1 == null) || (numTimesWordInPageArr2 == null)) {
      return rankedListUnsorted;
    }

    int numPossiblePages = pages.size();
    TaggedVertex<String> curPageTaggedVertex;
    int pageInDegree;
    int ranking;

    //iterate through the Integer array that represents how many times w appears in each page
    for (int i = 0; i < numPossiblePages; i++) {
      if ((numTimesWordInPageArr1[i] != null) && (numTimesWordInPageArr2[i] != null)) {
        curPageTaggedVertex = pages.get(i);
        pageInDegree = curPageTaggedVertex.getTagValue();
        Integer  numTimesWordInPage1 = numTimesWordInPageArr1[i];
        Integer  numTimesWordInPage2 = numTimesWordInPageArr2[i];
        if(numTimesWordInPage1 == null){
          numTimesWordInPage1 = 0;
        }
        if(numTimesWordInPage2 == null){
          numTimesWordInPage2 = 0;
        }
        ranking = (pageInDegree) * (numTimesWordInPage1 + numTimesWordInPage2);
        if (ranking > 0) {
          rankedListUnsorted.add(new TaggedVertex<>(curPageTaggedVertex.getVertexData(), ranking));
        }
      }
    }

    return getSortedList(rankedListUnsorted);
  }

  /**
   * Searches the index for pages containing at least one of the keywords w1
   * and w2.  Returns a list of qualifying
   * urls ordered by ranking (largest to smallest).  The tag
   * value associated with each url is its ranking.
   * The ranking for a given page is the number of occurrences
   * of w1 plus number of occurrences of w2, all multiplied by the
   * indegree of its url in the associated graph.
   * No pages with rank zero are included.
   *
   * @param w1 first keyword to search for
   * @param w2 second keyword to search for
   * @return ranked list of urls
   */
  public List<TaggedVertex<String>> searchWithOr(String w1, String w2) {

    if (w1 == null || w2 == null) {
      return null;
    }
    Integer[] numTimesWordInPageArr1 = indexHashMap.get(w1);
    Integer[] numTimesWordInPageArr2 = indexHashMap.get(w2);
    List<TaggedVertex<String>> rankedListUnsorted = new ArrayList<>();

    //if the word was never found, return empty
    if (numTimesWordInPageArr1 == null) {
      if (numTimesWordInPageArr2 == null) {
        return rankedListUnsorted;
      } else {
        return search(w2);
      }
    }
    if (numTimesWordInPageArr2 == null) {
      return search(w1);
    }
    int numPossiblePages = pages.size();

    TaggedVertex<String> curPageTaggedVertex;
    int pageInDegree;
    int ranking;

    //iterate through the Integer array that represents how many times w appears in each page
    for (int i = 0; i < numPossiblePages; i++) {
      if ((numTimesWordInPageArr1[i] != null) || (numTimesWordInPageArr2[i] != null)) {
        curPageTaggedVertex = pages.get(i);
        pageInDegree = curPageTaggedVertex.getTagValue();
        Integer  numTimesWordInPage1 = numTimesWordInPageArr1[i];
        Integer  numTimesWordInPage2 = numTimesWordInPageArr2[i];
        if(numTimesWordInPage1 == null){
          numTimesWordInPage1 = 0;
        }
        if(numTimesWordInPage2 == null){
          numTimesWordInPage2 = 0;
        }
        ranking = (pageInDegree) * (numTimesWordInPage1 + numTimesWordInPage2);
        if (ranking > 0) {
          rankedListUnsorted.add(new TaggedVertex<>(curPageTaggedVertex.getVertexData(), ranking));
        }
      }
    }
    return getSortedList(rankedListUnsorted);
  }

  /**
   * Searches the index for pages containing keyword w1
   * but NOT w2.  Returns a list of qualifying urls
   * ordered by ranking (largest to smallest).  The tag
   * value associated with each url is its ranking.
   * The ranking for a given page is the number of occurrences
   * of w1, multiplied by the
   * indegree of its url in the associated graph.
   * No pages with rank zero are included.
   *
   * @param w1 first keyword to search for
   * @param w2 second keyword to search for
   * @return ranked list of urls
   */
  public List<TaggedVertex<String>> searchAndNot(String w1, String w2) {

    if (w1 == null || w2 == null) {
      return null;
    }

    Integer[] numTimesWordInPageArr1 = indexHashMap.get(w1);
    Integer[] numTimesWordInPageArr2 = indexHashMap.get(w2);
    List<TaggedVertex<String>> rankedListUnsorted = new ArrayList<>();

    if (numTimesWordInPageArr1 == null) {
      return rankedListUnsorted;
    }
    if (numTimesWordInPageArr2 == null) {
      return search(w1);
    }

    int numPossiblePages = pages.size();
    TaggedVertex<String> curPageTaggedVertex;
    int pageInDegree;
    int ranking;

    //iterate through the Integer array that represents how many times w appears in each page
    for (int i = 0; i < numPossiblePages; i++) {
      if (numTimesWordInPageArr1[i] != null && numTimesWordInPageArr2[i] == null) {
        curPageTaggedVertex = pages.get(i);
        pageInDegree = curPageTaggedVertex.getTagValue();
        Integer  numTimesWordInPage1 = numTimesWordInPageArr1[i];
        if(numTimesWordInPage1 == null){
          numTimesWordInPage1 = 0;
        }
        ranking = (pageInDegree) * (numTimesWordInPage1);
        if (ranking > 0) {
          rankedListUnsorted.add(new TaggedVertex<>(curPageTaggedVertex.getVertexData(), ranking));
        }
      }
    }
    return getSortedList(rankedListUnsorted);
  }

  /**
   * This method will perform a merge sort on an unsorted list of Tagged
   * Vertex. This is used for ordering by ranking
   *
   * @param list an unsorted list of TaggedVertex<String>
   * @return A list of sorted TaggedVertex<String>
   */
  private List<TaggedVertex<String>> getSortedList(List<TaggedVertex<String>> list) {
    int size = list.size();
    int left = 0;
    int right = size - 1;

    TaggedVertex[] TVarr = new TaggedVertex[size];
    List<TaggedVertex<String>> result = new ArrayList<>();

    if (size > 0) {
      for (int i = 0; i < size; i++) {
        TVarr[i] = list.get(i);
      }
    }

    sort(TVarr, left, right);
    for (int i = 0; i < size; i++) {
      result.add(TVarr[i]);
    }

    return result;
  }

  /**
   * This method will perform a merge on an array of Tagged Vertex<String> based on Tagged
   * Values
   *
   * @param arr An array of TaggedVertex<String>
   * @param left index of the left most element
   * @param mid index of the middle element
   * @param right index of the right most element
   */
  private void merge(TaggedVertex<String>[] arr, int left, int mid, int right) {

    int sizeLeft = mid - left + 1;
    int sizeRight = right - mid;
    TaggedVertex<String>[] leftArr = new TaggedVertex[sizeLeft];
    TaggedVertex<String>[] rightArr = new TaggedVertex[sizeRight];

    for (int i = 0; i < sizeLeft; i++) {
      leftArr[i] = arr[left + i];
    }
    for (int j = 0; j < sizeRight; j++) {
      rightArr[j] = arr[mid + j + 1];
    }

    int i = 0;
    int j = 0;
    int k = left;
    while (i < sizeLeft && j < sizeRight) {
      if (leftArr[i].getTagValue() >= rightArr[j].getTagValue()) {
        arr[k] = leftArr[i];
        i++;
      } else {
        arr[k] = rightArr[j];
        j++;
      }
      k++;
    }
    while (i < sizeLeft) {
      arr[k] = leftArr[i];
      i++;
      k++;
    }
    while (j < sizeRight) {
      arr[k] = rightArr[j];
      j++;
      k++;
    }
  }

  /**
   * This method will recursively call itself and merge to perform the sorting
   * part of merge sort
   *
   * @param arr An array of TaggedVertex<String>
   * @param left index of the left most element
   * @param right index of the right most element
   */
  private void sort(TaggedVertex<String>[] arr, int left, int right) {
    if (right > left) {
      int mid = (left + right) / 2;
      sort(arr, left, mid);
      sort(arr, mid + 1, right);
      merge(arr, left, mid, right);
    }
  }
}