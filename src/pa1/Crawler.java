package pa1;

import api.Graph;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.UnsupportedMimeTypeException;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import api.TaggedVertex;

import java.io.IOException;
import java.util.*;

import static api.Util.ignoreLink;

/**
 * Implementation of a basic web crawler that creates a graph of some
 * portion of the world wide web.
 *
 * @author Zach Johnson and Alex Bjerke
 */
public class Crawler
{

  private String seed;
  private int maxDepth;
  private int maxPages;

  /**
   * Constructs a Crawler that will start with the given seed url, including
   * only up to maxPages pages at distance up to maxDepth from the seed url.
   * @param seedUrl
   * @param maxDepth
   * @param maxPages
   */
  public Crawler(String seedUrl, int maxDepth, int maxPages)
  {
    seed = seedUrl;
    this.maxDepth = maxDepth;
    this.maxPages = maxPages;
  }
  
  /**
   * Creates a web graph for the portion of the web obtained by a BFS of the 
   * web starting with the seed url for this object, subject to the restrictions
   * implied by maxDepth and maxPages.  
   * @return
   *   an instance of Graph representing this portion of the web
   */
  public Graph<String> crawl()
  {
    TaggedVertex<String> root = new TaggedVertex<>(seed, 1);    //Starting node to add to graph
    HashMap<String, Integer> discoveredList = new HashMap<>();          //List of discovered nodes
    int nextIndex = 1;                                                  //Index of the next node

    WebGraph<String> g = new WebGraph<>();                              //Graph of links
    g.vertexDataWithIncomingCounts().add(root);                         //Add starting node
    g.vertexData().add(seed);                                           //Add seedURL
    g.getOutgoingLists().add(new ArrayList<>());
    g.getIncomingLists().add(new ArrayList<>());
    Queue<String> Q = new PriorityQueue<>();                            //Queue for BFS
    g.setDepth(0, 0);                                       //Depth of seedURL

    Q.add(seed);
    discoveredList.put(seed, 0);                                        //Seed discovered

    int requestCount = 0;
    int pageCount = 1;

    while (!Q.isEmpty()) {
      String parentLink = Q.remove();                   //Get first link from Q
      int parentIndex = discoveredList.get(parentLink); //Get index of parentLink
      int currentDepth = g.getDepth(parentIndex);       //Get depth of parentLink

      Document doc = new Document("");

      try {
          requestCount++;
          if (requestCount >= 50) {
              Thread.sleep(3000);         // Wait for 3 seconds to be courteous
              requestCount = 0;
          }
          doc = Jsoup.connect(parentLink).get();        //Get the webpage from the URL
      }
      catch (UnsupportedMimeTypeException e)
      {
          System.out.println("--unsupported document type, do nothing");
      }
      catch (HttpStatusException e)
      {
          System.out.println("--invalid link, do nothing");
      }
      catch (IOException e) {
          e.printStackTrace();
      }
      catch (InterruptedException ignore) {
      }

      Elements links = doc.select("a[href]");   //Get list of links
      HashSet<String> noDuplicates = new HashSet<>();       //HashSet for filtering duplicates
      noDuplicates.add(parentLink);                         //Add parentLink to noDuplicates

      for (Element link : links) {
        String childLink = link.attr("abs:href");   //Remove unnecessary HTML stuff

        if (!ignoreLink(parentLink, childLink)) {   //Ignore bad links
          if (!noDuplicates.contains(childLink)) {  //Filter duplicates

            noDuplicates.add(childLink);    //Add childLink to noDuplicates

            if ((discoveredList.get(childLink) == null) && (pageCount < maxPages) && (currentDepth < maxDepth)) { //If node not discovered and doesn't violate the limits
              TaggedVertex<String> node = new TaggedVertex<>(childLink, 0); //New node for childLink
              Q.add(childLink);                                         //Add childLink to Q
              discoveredList.put(childLink, nextIndex); //Discover childLink
              g.vertexData().add(childLink);            //Add childLink to vertexData
              g.vertexDataWithIncomingCounts().add(node);   //Add childLink node
              g.getOutgoingLists().add(new ArrayList<>());  //Add outgoingList for childLink
              g.getIncomingLists().add(new ArrayList<>());  //Add incomingList for childLink
              g.setDepth(nextIndex, currentDepth + 1);  //Set depth to 1 more than parentLink
              pageCount++;
              nextIndex++;  //Update index for next link
            }
            if (discoveredList.get(childLink) != null) {
                g.addNeighbor(parentIndex, discoveredList.get(childLink));  //Add outgoing edge from parentLink to childLink and incoming from childLink to parentLink
            }
          }
        }
      }
    }
    return g;
  }
}
