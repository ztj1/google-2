package test;

import api.Graph;
import api.TaggedVertex;
//import org.junit.Test;
import pa1.Crawler;
import pa1.Index;
import pa1.WebGraph;

import java.util.ArrayList;
import java.util.List;

public class main {

    public static void main(String[] args){
        System.out.println("Starting the crawl");
        Crawler crawler = new Crawler("https://www.wikipedia.org/", 10, 50); // https://www.york.ac.uk/teaching/cws/wws/webpage1.html
        Graph<String> G;
        long millis = System.currentTimeMillis();
        G = crawler.crawl();
        long timeTaken = System.currentTimeMillis() - millis;
        for (int i = 0; i < G.vertexData().size(); i++) {
            System.out.println(G.vertexData().get(i) + " incoming: " + G.vertexDataWithIncomingCounts().get(i).getTagValue() +
                    " outgoing: " + G.getNeighbors(i).size());
        }
        System.out.println("Crawl took " + timeTaken +" ms");
        System.out.println();
        System.out.println("Starting indexing");
        Index index = new Index(G.vertexDataWithIncomingCounts());
        millis = System.currentTimeMillis();
        index.makeIndex();
        timeTaken = System.currentTimeMillis() - millis;
        System.out.println("MakeIndex took " + timeTaken +" ms");
        System.out.println();

        System.out.println("Starting searching for the word 'tag'");
        millis = System.currentTimeMillis();
        List<TaggedVertex<String>> result = index.search("tag");
        timeTaken = System.currentTimeMillis() - millis;
        System.out.println("Searching took " + timeTaken +" ms");

        System.out.println();


        for(int i = 0; i < result.size(); i++){
            System.out.println(i + ": " + result.get(i).getVertexData() +" -- " + result.get(i).getTagValue() + " times");
        }

        System.out.println("Starting searching for the word 'a'");
        millis = System.currentTimeMillis();
        List<TaggedVertex<String>> simpleResult = index.search("a");
        timeTaken = System.currentTimeMillis() - millis;
        System.out.println("Searching took " + timeTaken +" ms");

        System.out.println();


        for(int i = 0; i < simpleResult.size(); i++){
            System.out.println(i + ": " + simpleResult.get(i).getVertexData() +" -- " + simpleResult.get(i).getTagValue() + " times");
        }

        System.out.println();

        System.out.println("Starting searching for 'header'");
        millis = System.currentTimeMillis();
        List<TaggedVertex<String>> emptyResult = index.search("header");
        timeTaken = System.currentTimeMillis() - millis;
        System.out.println("Searching took " + timeTaken +" ms");

        System.out.println();


        for(int i = 0; i < emptyResult.size(); i++){
            System.out.println(i + ": " + emptyResult.get(i).getVertexData() +" -- " + emptyResult.get(i).getTagValue() + " times");
        }


        System.out.println();

        System.out.println("Starting AND searching for the words 'tag' and 'header'");
        millis = System.currentTimeMillis();
        List<TaggedVertex<String>> andResult = index.searchWithAnd("tag", "header");
        timeTaken = System.currentTimeMillis() - millis;
        System.out.println("Searching took " + timeTaken +" ms");

        for(int i = 0; i < andResult.size(); i++){
            System.out.println(i + ": " + andResult.get(i).getVertexData() +" -- " + andResult.get(i).getTagValue() + " times");
        }

        System.out.println();

        System.out.println("Starting OR searching for the words 'tag' or 'header'");
        millis = System.currentTimeMillis();
        List<TaggedVertex<String>> orResult = index.searchWithOr("tag", "header");
        timeTaken = System.currentTimeMillis() - millis;
        System.out.println("Searching took " + timeTaken +" ms");

        System.out.println();

        for(int i = 0; i < orResult.size(); i++){
            System.out.println(i + ": " + orResult.get(i).getVertexData() +" -- " + orResult.get(i).getTagValue() + " times");
        }

        System.out.println();

        System.out.println("Starting AND NOT searching for the words 'tag' and not 'header'");
        millis = System.currentTimeMillis();
        List<TaggedVertex<String>> andNotResult = index.searchAndNot("tag", "header");
        timeTaken = System.currentTimeMillis() - millis;
        System.out.println("Searching took " + timeTaken +" ms");

        for(int i = 0; i < andNotResult.size(); i++){
            System.out.println(i + ": " + andNotResult.get(i).getVertexData() +" -- " + andNotResult.get(i).getTagValue() + " times");
        }

        System.out.println();

        //index.printIndex();

        ArrayList<String> arr = G.vertexData();
        System.out.println(arr.toString());
    }
}
