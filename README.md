# Web Crawler

A web crawler application created by Zach Johnson and Alex Bjerke for COM S 311. The application uses BFS to generate a graph of web pages, starting from a seed URL and creating edges to all pages linked from that page (pages downloaded using jsoup). The graph generating method also has parameters "maxPages" and "maxDepth" to constrain the size of the graph.

The graph is passed to method that creates an inverted index containing the URLs, their indegrees, and a list of the words on each webpage (excluding unimportant "stop" words). 

The inverted index is used to implement search methods "search", "searchWithAnd", "searchWithOr", and "searchAndNot". The method "search" only takes one parameter (a string), and returns a list of webpages ordered according to their computed importance (based on indegree and number of matching words). The other 3 methods take two parameters (both strings) and searches for pages that match the first string "and"/"or"/"and not" the second string.

Note: we received a 550/500 on this project for correctness and time/space efficiency.